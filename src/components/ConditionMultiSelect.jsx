import "../css/cascade.css";
import PropTypes from 'prop-types';
import {Component} from "rainbowui-desktop-core";
import { _ } from "core-js";

export default class ConditionMultiSelect extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedValues: [],
      selectConditions: []
      // componentWillReceivePropsFlag : false
    };
  }

  componentDidMount() {
    var self = this;
    if (self.props.selected) {
      $.each(self.props.selected, function (index, data) {
        self.addSelectRecord(data);
      });
    }

    document.onclick = function (event){
      if (event.target.className.indexOf("multiDropDown") < 0) {
        const showCollapse = $('.multiSelectCollapse.collapse.show');
        if(!(showCollapse && showCollapse[0] && showCollapse[0].id && event.target.href && event.target.href.indexOf(showCollapse[0].id) > -1)){
          showCollapse.collapse('hide');
        }
        if (event.target.className.indexOf('link_active') < 0  && event.target.localName != 'a') {
          $('.panel.panel-li').removeClass('activeli');
        }
      }
    }
  }

  componentWillMount() {
    this.componentId = (this.props.id) ? this.props.id : "multiSelect0";
  }

  componentWillReceiveProps(newprop){
    var self = this;
    if(self.props.selected != newprop.selected){
      //self.state.componentWillReceivePropsFlag = true ;
       //if(this.state.componentWillReceivePropsFlag){
        //close the flag
        //this.state.componentWillReceivePropsFlag = false;
        //clear the selected elements from component on page
        $.each(this.state.selectedValues , function ( index , data ) {
              var levels = data.split("_");
              self.delSelectRecord(levels[0], levels[1], levels[2]);
        });
        //clean selected array
        this.state.selectedValues = [];
      //}
      if (newprop.selected) {
        $.each(newprop.selected, function (index, data) {
          self.addSelectRecord(data);
        });
      }
    }
  }

  render() {
    if (!this.props.dataSource) {
      return null;
    }
    let body = [];
    var _self = this;
      body.push(
      <div className="condition_box">
        <span className="selected_title"></span>
        {_self.getSelected()}
      </div>);
    $.each(this.props.dataSource, function (index, element) {
      body.push(
        <div className="multi">
     
            <div className="condition_title">
              <label className="mulit-label">{element.Name + ":"}</label></div>
            <div>
              <ol className="sort">
                <div className="panel-group" id={_self.componentId + "-" + element.Id}>
                  {_self.getEachChildSource(element)}
                  {_self.getEachDetail(element)}
                </div>
              </ol>
            </div>
    
        </div>
      );
    });
  
    return (
        <div>
              <div class="conditionMultiSelect-div">
                {body}
              </div>
              <div style={{clear:'both'}}></div>
        </div>
    );
  }

  openItem(id) {
    $('.panel.panel-li').removeClass('activeli')
    $('#'+id+'_li').addClass('activeli')
    $('#'+id).removeClass('collapsing');
  }
  
  getEachChildSource(data) {
    let body = [];
    var _self = this;
    if(data.Child){
      $.each(data.Child, function (index, element) {
	      body.push(

	        <div className="panel panel-li" id={_self.componentId + "-select-detail-" + data.Id + "_" + element.Id+'_li'}>
	          <div className="panel-heading">
              <a href={"#" + _self.componentId + "-select-detail-" + data.Id + "_" + element.Id} onClick={_self.openItem.bind(_self, _self.componentId + "-select-detail-" + data.Id + "_" + element.Id)}
              style={{color:'#000'}}
	               data-parent={"#" + _self.componentId + "-" + data.Id} data-toggle="collapse">
	              {element.Name}
	            </a>
	          </div>
            
	        </div>
          

        );
        
	    });
	  }
    return body;
  }

  getEachDetail(data) {
    let body = [];
    var _self = this;
    if(data.Child){
      $.each(data.Child, function (index, element) {
      body.push(
        <div id={_self.componentId + "-select-detail-" + data.Id + "_" + element.Id}
              className={"panel-collapse collapse multiDropDown multiSelectCollapse"}>
          <ol className="multiDropDown">
            {_self.getEachSubSource(data, element)}
          </ol>
        </div>
      )
    });
    }
    return body;
  }

  getEachSubSource(data, child) {
    let body = [];
    var _self = this;
    if (data.SelectAll) {
      body.push(
        <li className="multiDropDown">
          <input type="checkbox" id={this.componentId + "-select-detail-" + data.Id + "_" + child.Id + "_ALL"}
                 name={this.componentId + "-select-collection-" + data.Id + "_" + child.Id + "_ALL"}
                 onChange={this.clickCheckBox.bind(this, data.Id, child.Id, "ALL")}
                 ref={ data.Id + "_" + child.Id + "-ALL"} className="multiDropDown"/>
          <label className="multiDropDown">{data.SelectAll}</label>
        </li>
      );
    }

		if(child.Child){
			$.each(child.Child, function (index, element) {
      body.push(
        <li className="multiDropDown">
          <input type="checkbox" className="multiDropDown"
                 id={_self.componentId + "-select-detail-" + data.Id + "_" + child.Id + "_" + element.Id}
                 name={_self.componentId + "-select-collection-" + data.Id + "_" + child.Id}
                 ref={data.Id + "_" + child.Id + "_" + element.Id}
                 onChange={_self.clickCheckBox.bind(_self, data.Id, child.Id, element.Id)}/>&nbsp;
          <label className="multiDropDown">{element.Name}</label>
        </li>
      );
    });
   return body;
		}
  }

  getSelected() {
    var selectedValues = this.state.selectedValues;
    var body = [];
    var arr = [];
    var dataSource = this.props.dataSource;
    var _self = this;
    $.each(selectedValues, function (index, e) {
      if (e) {
        var levels = e.split("_");
        var level0 = _self.searchById(dataSource, levels[0]);
        var level1 = _self.searchById(level0.Child, levels[1]);
        if (levels[2] == "ALL") {
          body.push(
            <div>
              <label>{level0.Name + ": " + level1.Name + " - " + level0.SelectAll}</label>
              <a className="clearX" href="javascript:void (0);"
                 onClick={_self.delSelectRecord.bind(_self, levels[0], levels[1], levels[2])}>X</a>
            </div>
          );
          arr.push(level1.Name)
        } else {
          var level2 = _self.searchById(level1.Child, levels[2]);
          body.push(
            <div>
              <label>{level0.Name + ": " + level1.Name + " - " + level2.Name}</label>
              <a className="clearX" href="javascript:void (0);"
                 onClick={_self.delSelectRecord.bind(_self, levels[0], levels[1], levels[2])}>X</a>
            </div>
          );
          arr.push(level1.Name)
        }
      }
    });
    _.uniq(arr);
    if (arr.length > 0) {
      let arrDev =  $('.conditionMultiSelect-div').find('a');
      arrDev.each(function(inde, e) {
        $(e).css({'color':'#000'})
      });
      arrDev.each(function(inde, e) {
          arr.forEach(function(i, dd) {
            if ($(e).text() == i) {
                $(e).css({'color':'#2D8CF0'})
            }
          })
          
      });
    }
    return body;
  }

  searchById(list, id) {
    for (var i = 0; i < list.length; i++) {
      if (list[i].Id == id) {
        return list[i];
      }
    }
  }

  clickCheckBox(data, child, elementId) {
    let selected = this.state.selectedValues;
    let selectConditions = this.state.selectConditions;
    var _self = this;
    var v_item = document.getElementById(this.componentId + "-select-detail-" + data + "_" + child + "_" + elementId);
    var dataSource = this.props.dataSource;
    var level0 = _self.searchById(dataSource, data);
    var level1 = _self.searchById(level0.Child, child);
    var level2 = _self.searchById(level1.Child, elementId);
    var level1Name = level1.Obj.Code == null? level1.Obj : level1.Obj.Code;
    if(level2 == undefined){
      var level2Name = elementId
    }else{
      var level2Name = level2.Name.replace(/\(.*?\)/g,'');
    }
    var condition = level1Name + "_" + level2Name;
    if (elementId == "ALL") {
      var items = document.getElementsByName(this.componentId + "-select-collection-" + data + "_" + child);
      $.each(items, function (index, e) {
        if (v_item.checked) {
          e.checked = true;
          e.disabled = true;
          selected = _self.unselectRecord(data + "_" + child + "_", selected);
          selectConditions = _self.unselectRecord(condition, selectConditions);
        }
        else {
          e.checked = false;
          e.disabled = false;
        }
      });
    }
    if (v_item.checked) {
      selected = this.selectRecord(data + "_" + child + "_" + elementId, selected);
      selectConditions = _self.selectRecord(condition, selectConditions);
    }
    else {
      selected = this.unselectRecord(data + "_" + child + "_" + elementId, selected);
      selectConditions = _self.unselectRecord(condition, selectConditions);
    }
    var selectConditionStr = selectConditions.join('_');
    this.setState({selectedValues: selected, selectConditions: selectConditions});
    this.props.onSelect(selected, selectConditionStr);
  }

  selectRecord(record, selected) {
    var self =this;
    if (!selected) {
      selected = [];
    }
    //push new 
    selected.push(record);
    selected.sort();
    return selected;
  }
  

  unselectRecord(record, selected) {
    let newSelected = [];
    if (selected) {
      $.each(selected, function (index, e) {
        if (e && e.indexOf(record) < 0) {
          newSelected.push(e);
        }
      });
    }
    return newSelected;
  }

  delSelectRecord(data, child, element) {
    var v_item = document.getElementById(this.componentId + "-select-detail-" + data + "_" + child + "_" + element);
    v_item.checked = false;
    this.clickCheckBox(data, child, element);
  }

  addSelectRecord(data) {
    var v_item = document.getElementById(this.componentId + "-select-detail-" + data);
    if (v_item) {
      v_item.checked = true;
      var levels = data.split("_");
      this.clickCheckBox(levels[0], levels[1], levels[2]);
    }
  }
}

/**
 * ConditionMultiSelect component prop types
 */
ConditionMultiSelect.propTypes = $.extend({}, Component.propTypes, {
  id: PropTypes.string,
  dataSource: PropTypes.array.isRequired,
  onSelect: PropTypes.func,
  selected: PropTypes.array
});

ConditionMultiSelect.defaultProps = $.extend({}, Component.defaultProps, {
  onSelect: () => {
  },
});